FROM arm32v7/debian:bullseye

ARG DEBIAN_FRONTEND=noninteractive
RUN echo "deb http://deb.debian.org/debian/ bullseye main contrib non-free\ndeb-src http://deb.debian.org/debian bullseye main\ndeb http://deb.debian.org/debian buster main contrib non-free" > /etc/apt/sources.list
RUN apt update -y && apt upgrade -y
RUN apt build-dep -y mesa
RUN apt update -y && apt install -y \
	git \
	wget \
	cmake \
	build-essential \
	meson \
	python3 \
	gcc \
	pciutils \
	libnm0 \
	zenity \
	nginx \
	libgtk2.0-0 \
	libdbus-glib-1-2 \
	libxss1 \
	libdbusmenu-gtk4 \
	libsdl2-2.0-0 \
	libice6 \
	libsm6 \
	libopenal1 \
	libusb-1.0-0 \
	libappindicator1 \
	mesa-utils

RUN mkdir /build

RUN git clone https://gitlab.freedesktop.org/mesa/mesa -b 21.1 /build/mesa
RUN cd /build/mesa; meson build \
	--prefix=/usr/ \
        -Degl=true \
        -Dgbm=true \
        -Dgles1=false \
        -Dgles2=true \
        -Dopengl=true \
        -Ddri3=true  \
        -Dgallium-drivers=swrast,virgl \
        -Dgbm=false \
        -Dglx=dri \
        -Dllvm=true \
        -Dlmsensors=false \
        -Dshared-glapi=true \
        -Dshared-llvm=true \
	-Dvulkan-drivers=virtio-experimental,swrast; \
	ninja -C build -j$(($(nproc) -1)); ninja -C build install

RUN git clone https://github.com/ptitSeb/box86 /build/box86
RUN cd /build/box86; mkdir build; cd build; cmake .. -DRPI4=1; make -j$(($(nproc) -1)); make install
RUN cd /build; wget http://media.steampowered.com/client/installer/steam.deb; mkdir /build/steam; ar x steam.deb; tar xf data.tar.xz -C /build/steam;

RUN adduser user
USER user
WORKDIR /home/user
ENV LIBGL_ALWAYS_SOFTWARE=1 GALLIUM_DRIVER=llvmpipe DISPLAY=:0

CMD setarch -L linux32 /build/steam/usr/lib/steam/bin_steam.sh
